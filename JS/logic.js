// Seleción de los elementos INDEX
 let inputUserName = document.querySelector('#userName');
 let userNameButton = document.querySelector('.userName');

//  Declaración de variable INDEX
 let userName = '';

// Gestión de cookies
function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0;i < ca.length;i++) {
		var c = ca[i];
		while (c.charAt(0)==' ') c = c.substring(1,c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
	}
	return null;
}
 function keepUserName() {
    if (inputUserName.value != '') {
    document.cookie = 'userName=' + inputUserName.value;
    alert('Hola ' + readCookie('userName') + ', listo para empeza?');
     }
    }

// Seleción de los elementos CROSSWORD
let inputLetter1 = document.querySelector('.input1');
let divLetter1 = document.querySelector('.letter1');
let inputLetter2 = document.querySelector('.input2');
let divLetter2 = document.querySelector('.letter2');
let inputLetter3 = document.querySelector('.input3');
let divLetter3 = document.querySelector('.letter3');
let inputLetter4 = document.querySelector('.input4');
let divLetter4 = document.querySelector('.letter4');
let inputLetter5 = document.querySelector('.input5');
let divLetter5 = document.querySelector('.letter5');
let inputLetter6 = document.querySelector('.input6');
let divLetter6 = document.querySelector('.letter6');
let inputLetter7 = document.querySelector('.input7');
let divLetter7 = document.querySelector('.letter7');
let inputLetter8 = document.querySelector('.input8');
let divLetter8 = document.querySelector('.letter8');
let inputLetter9 = document.querySelector('.input9');
let divLetter9 = document.querySelector('.letter9');
let inputLetter10 = document.querySelector('.input10');
let divLetter10 = document.querySelector('.letter10');
let inputLetter11 = document.querySelector('.input11');
let divLetter11 = document.querySelector('.letter11');
let inputLetter12 = document.querySelector('.input12');
let divLetter12 = document.querySelector('.letter12');
let inputLetter13 = document.querySelector('.input13');
let divLetter13 = document.querySelector('.letter13');

let message = document.querySelector('.message');

let sendButton = document.querySelector('.send');
let showClueButton = document.querySelector('.showClue');
let hideClueButton = document.querySelector('.hideClue');

// Declaración de variables CROSSWORD
let letter1 = divLetter1.textContent;
let letter2 = divLetter2.textContent;
let letter3 = divLetter3.textContent;
let letter4 = divLetter4.textContent;
let letter5 = divLetter5.textContent;
let letter6 = divLetter6.textContent;
let letter7 = divLetter7.textContent;
let letter8 = divLetter8.textContent;
let letter9 = divLetter9.textContent;
let letter10 = divLetter10.textContent;
let letter11 = divLetter11.textContent;
let letter12 = divLetter12.textContent;
let letter13 = divLetter13.textContent;
inputLetter1.value = '';
inputLetter2.value = '';
inputLetter3.value = '';
inputLetter4.value = '';
inputLetter5.value = '';
inputLetter6.value = '';
inputLetter7.value = '';
inputLetter8.value = '';
inputLetter9.value = '';
inputLetter10.value = '';
inputLetter11.value = '';
inputLetter12.value = '';
inputLetter13.value = '';
let inputLetter;
let findOut1 = true;
let findOut2 = true;
let findOut3 = true;
let findOut4 = true;
let findOut5 = true;
let findOut6 = true;
let findOut7 = true;
let findOut8 = true;
let findOut9 = true;
let findOut10 = true;
let findOut11 = true;
let findOut12 = true;
let findOut13 = true;

// FUNCIONES
function endGame() {
    if(findOut1 === false && findOut2 === false && findOut3 === false && findOut4 === false
       && findOut5 === false && findOut6 === false && findOut7 === false && findOut8 === false
       && findOut9 === false && findOut10 === false && findOut11 === false && findOut12 === false
       && findOut13 === false) {
            alert('ENORABUENA ' + readCookie('userName') + ' HAS GANADO !!!')
    } 
    
}

function compareLetter1() {
    inputLetter = (inputLetter1.value).toUpperCase();

    if (inputLetter === letter1) {
        inputLetter1.style.position = "initial";
        divLetter1.style.backgroundColor = 'green';
        findOut1 = false;
    }
    else {
        inputLetter1.style.borderColor = "red";
        inputLetter1.value = '';
    }
}

function compareLetter2() {
    inputLetter = (inputLetter2.value).toUpperCase();

    if (inputLetter === letter2) {
        inputLetter2.style.position = "initial";
        divLetter2.style.backgroundColor = 'green';
        findOut2 = false;
    }
    else {
        inputLetter2.style.borderColor = "red";
        inputLetter2.value = '';
    }    
}

function compareLetter3() {
    inputLetter = (inputLetter3.value).toUpperCase();

    if (inputLetter === letter3) {
        inputLetter3.style.position = "initial";
        divLetter3.style.backgroundColor = 'green';
        findOut3 = false;
    }
    else {
        inputLetter3.style.borderColor = "red";
        inputLetter3.value = '';
    }    
}

function compareLetter4() {
    inputLetter = (inputLetter4.value).toUpperCase();

    if (inputLetter === letter4) {
        inputLetter4.style.position = "initial";
        divLetter4.style.backgroundColor = 'green';
        findOut4 = false;
    }
    else {
        inputLetter4.style.borderColor = "red";
        inputLetter4.value = '';
    }    
}

function compareLetter5() {
    inputLetter = (inputLetter5.value).toUpperCase();

    if (inputLetter === letter5) {
        inputLetter5.style.position = "initial";
        divLetter5.style.backgroundColor = 'green';
        findOut5 = false;
    }
    else {
        inputLetter5.style.borderColor = "red";
        inputLetter5.value = '';
    }    
}

function compareLetter6() {
    inputLetter = (inputLetter6.value).toUpperCase();

    if (inputLetter === letter6) {
        inputLetter6.style.position = "initial";
        divLetter6.style.backgroundColor = 'green';
        findOut6 = false;
    }
    else {
        inputLetter6.style.borderColor = "red";
        inputLetter6.value = '';
    }    
}

function compareLetter7() {
    inputLetter = (inputLetter7.value).toUpperCase();

    if (inputLetter === letter7) {
        inputLetter7.style.position = "initial";
        divLetter7.style.backgroundColor = 'green';
        findOut7 = false;
    }
    else {
        inputLetter7.style.borderColor = "red";
        inputLetter7.value = '';
    }    
}

function compareLetter8() {
    inputLetter = (inputLetter8.value).toUpperCase();

    if (inputLetter === letter8) {
        inputLetter8.style.position = "initial";
        divLetter8.style.backgroundColor = 'green';
        findOut8 = false;
    }
    else {
        inputLetter8.style.borderColor = "red";
        inputLetter8.value = '';
    }    
}

function compareLetter9() {
    inputLetter = (inputLetter9.value).toUpperCase();

    if (inputLetter === letter9) {
        inputLetter9.style.position = "initial";
        divLetter9.style.backgroundColor = 'green';
        findOut9 = false;
    }
    else {
        inputLetter9.style.borderColor = "red";
        inputLetter9.value = '';
    }    
}

function compareLetter10() {
    inputLetter = (inputLetter10.value).toUpperCase();

    if (inputLetter === letter10) {
        inputLetter10.style.position = "initial";
        divLetter10.style.backgroundColor = 'green';
        findOut10 = false;
    }
    else {
        inputLetter10.style.borderColor = "red";
        inputLetter10.value = '';
    }    
}

function compareLetter11() {
    inputLetter = (inputLetter11.value).toUpperCase();

    if (inputLetter === letter11) {
        inputLetter11.style.position = "initial";
        divLetter11.style.backgroundColor = 'green';
        findOut11 = false;
    }
    else {
        inputLetter11.style.borderColor = "red";
        inputLetter11.value = '';
    }    
}

function compareLetter12() {
    inputLetter = (inputLetter12.value).toUpperCase();

    if (inputLetter === letter12) {
        inputLetter12.style.position = "initial";
        divLetter12.style.backgroundColor = 'green';
        findOut12 = false;
    }
    else {
        inputLetter12.style.borderColor = "red";
        inputLetter12.value = '';
    }    
}

function compareLetter13() {
    inputLetter = (inputLetter13.value).toUpperCase();

    if (inputLetter === letter13) {
        inputLetter13.style.position = "initial";
        divLetter13.style.backgroundColor = 'green';
        findOut13 = false;
    }
    else {
        inputLetter13.style.borderColor = "red";
        inputLetter13.value = '';
    }    
}

function showClue1() {
    if(inputLetter1.style.position = "relative") {
    inputLetter1.style.position = "initial";
    }
}
function showClue2() {
    if(inputLetter2.style.position = "relative") {
    inputLetter2.style.position = "initial";
    }
}
function showClue3() {
    if(inputLetter3.style.position = "relative") {
    inputLetter3.style.position = "initial";
    }
}
function showClue4() {
    if(inputLetter4.style.position = "relative") {
    inputLetter4.style.position = "initial";
    }
}
function showClue5() {
    if(inputLetter5.style.position = "relativel") {
    inputLetter5.style.position = "initial";
    }
}
function showClue6() {
    if(inputLetter6.style.position = "relative") {
    inputLetter6.style.position = "initial";
    }
}
function showClue7() {
    if(inputLetter7.style.position = "relative") {
    inputLetter7.style.position = "initial";
    }
}
function showClue8() {
    if(inputLetter8.style.position = "relative") {
    inputLetter8.style.position = "initial";
    }
}
function showClue9() {
    if(inputLetter9.style.position = "relative") {
        inputLetter9.style.position = "initial";
        }
}
function showClue10() {
    if(inputLetter10.style.position = "relative") {
    inputLetter10.style.position = "initial";
    }
}
function showClue11() {
    if(inputLetter11.style.position = "relative") {
    inputLetter11.style.position = "initial";
    }
}
function showClue12() {
    if(inputLetter12.style.position = "relative") {
    inputLetter12.style.position = "initial";
    }
}
function showClue13() {
    if(inputLetter13.style.position = "relative") {
    inputLetter13.style.position = "initial";
    }
}

function hideClue() {

    inputLetter1.removeEventListener('click', showClue1);
    inputLetter2.removeEventListener('click', showClue2);
    inputLetter3.removeEventListener('click', showClue3);
    inputLetter4.removeEventListener('click', showClue4);
    inputLetter5.removeEventListener('click', showClue5);
    inputLetter6.removeEventListener('click', showClue6);
    inputLetter7.removeEventListener('click', showClue7);
    inputLetter8.removeEventListener('click', showClue8);
    inputLetter9.removeEventListener('click', showClue9);
    inputLetter10.removeEventListener('click', showClue10);
    inputLetter11.removeEventListener('click', showClue11);
    inputLetter12.removeEventListener('click', showClue12);
    inputLetter13.removeEventListener('click', showClue13);   
    if(findOut1) {
    inputLetter1.style.position = "relative";
    }
    if(findOut2) {
    inputLetter2.style.position = "relative";
    }
    if(findOut3) {
        inputLetter3.style.position = "relative";
    }
    if(findOut4) {
        inputLetter4.style.position = "relative";
    }
    if(findOut5) {
        inputLetter5.style.position = "relative";
    }
    if(findOut6) {
        inputLetter6.style.position = "relative";
    }
    if(findOut7) {
        inputLetter7.style.position = "relative";
    }
    if(findOut8) {
        inputLetter8.style.position = "relative";
    }
    if(findOut9) {
        inputLetter9.style.position = "relative";
    }
    if(findOut10) {
        inputLetter10.style.position = "relative";
    }
    if(findOut11) {
        inputLetter11.style.position = "relative";
    }
    if(findOut12) {
        inputLetter12.style.position = "relative";
    }
    if(findOut13) {
        inputLetter13.style.position = "relative";
    }
        
}

function allowShowClue() {
    inputLetter1.addEventListener('click', showClue1);
    inputLetter2.addEventListener('click', showClue2);
    inputLetter3.addEventListener('click', showClue3);
    inputLetter4.addEventListener('click', showClue4);
    inputLetter5.addEventListener('click', showClue5);
    inputLetter6.addEventListener('click', showClue6);
    inputLetter7.addEventListener('click', showClue7);
    inputLetter8.addEventListener('click', showClue8);
    inputLetter9.addEventListener('click', showClue9);
    inputLetter10.addEventListener('click', showClue10);
    inputLetter11.addEventListener('click', showClue11);
    inputLetter12.addEventListener('click', showClue12);
    inputLetter13.addEventListener('click', showClue13);
}

function showMessage() {
    message.style.display = 'block';
}

function hideMessage() {
    message.style.display = 'none';
}



// Evenementos CROSSWORDS
sendButton.addEventListener('click', compareLetter1);
sendButton.addEventListener('click', compareLetter2);
sendButton.addEventListener('click', compareLetter3);
sendButton.addEventListener('click', compareLetter4);
sendButton.addEventListener('click', compareLetter5);
sendButton.addEventListener('click', compareLetter6);
sendButton.addEventListener('click', compareLetter7);
sendButton.addEventListener('click', compareLetter8);
sendButton.addEventListener('click', compareLetter9);
sendButton.addEventListener('click', compareLetter10);
sendButton.addEventListener('click', compareLetter11);
sendButton.addEventListener('click', compareLetter12);
sendButton.addEventListener('click', compareLetter13);
sendButton.addEventListener('click', endGame);

showClueButton.addEventListener('click', allowShowClue);
showClueButton.addEventListener('mouseover', showMessage);
showClueButton.addEventListener('mouseout', hideMessage);
hideClueButton.addEventListener('click', hideClue);






